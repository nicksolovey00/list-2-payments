import java.util.Objects;

abstract public class BankUser implements Cloneable{
    private Date date;
    private String name;
    public BankUser(String name,int day,int month, int year){
        this.name = name;
        this.date = new Date(day, month, year);
    }

    public BankUser(String name, String date){
        this.name = name;
        this.date = new Date(date);
    }
    public BankUser(String name, Date date){
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public Date getDate(){
        return date;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BankUser{");
        sb.append("date=").append(date);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankUser bankUser = (BankUser) o;
        return Objects.equals(date, bankUser.date) &&
                Objects.equals(name, bankUser.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, name);
    }
}
