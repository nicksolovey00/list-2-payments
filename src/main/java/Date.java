import java.util.Objects;

public class Date implements Cloneable{
    private int day, month, year;
    {
        day = 0;
        month = 0;
        year = 0;
    }
    public Date(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Date(String date){
        setDateFromString(date);
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setDateFromString(String date){
        int[] tmpDate = new int[3];
        String[] tmpDateString = date.split("-");
        if(tmpDateString.length > 3){
            throw new IllegalArgumentException("String is out of format dd.mm.yy");
        }
        for(int i = 0;i < 3;++i){
            tmpDate[i] = Integer.parseInt(tmpDateString[i]);
        }
        this.day = tmpDate[0];
        this.month = tmpDate[1];
        this.year = tmpDate[2];
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Date{");
        sb.append("day=").append(day);
        sb.append(", month=").append(month);
        sb.append(", year=").append(year);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Date date = (Date) o;
        return day == date.day &&
                month == date.month &&
                year == date.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, month, year);
    }
}
